## Docker + node web app 


##### Dependencies: docker

##### run:

```
//create image
docker build -t <tagname>

//show image
docker images

//now run the image
docker run -p 8000:8000 -d <tagname>

//log the app
docker ps
docker logs <contanerid>

//test in culr or open in navigator 

curl -i localhost:8000

```